package me.bumblebeee_.servermute;

import java.util.ArrayList;
import java.util.List;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

//Problem with list

public class Servermute extends JavaPlugin implements Listener {
	
	boolean status = false;
	List<String> players = new ArrayList<String>();
	
	public void onEnable() {
		
		saveDefaultConfig();
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String args[]) {
		
		if (cmd.getName().equalsIgnoreCase("servermute")) {
			if (sender.hasPermission("servermute.servermute")) {				
				if (args.length > 0) {
					if (args[0].equalsIgnoreCase("reload")) {
						if (sender.hasPermission("servermute.reload")) {
							try {
								reloadConfig();
								sender.sendMessage(ChatColor.GREEN + "Config successfully reloaded!");
							} catch (Exception e1) {
								e1.printStackTrace();
								sender.sendMessage(ChatColor.RED + "Error while reloading config, Check console for more info");
							}
							
							return true;
						} else {
							sender.sendMessage(ChatColor.RED + "You do not have the required permissions!");
							return true;
						}
					}
					if (args[0].equalsIgnoreCase("on")) {
						if (status == false) {
							status = true;
							Bukkit.getServer().broadcastMessage(ChatColor.RED + "Server has been muted by " + ChatColor.GREEN + sender.getName() + "!");
							return true;
						} else if (status == true) {
							sender.sendMessage(ChatColor.RED + "Server is already muted!");
							return true;
						}
					} else if (args[0].equalsIgnoreCase("off")) {
						if (status == true) {
							players.clear();
							status = false;
							Bukkit.getServer().broadcastMessage(ChatColor.RED + "Server has been unmuted by " + ChatColor.GREEN + sender.getName() + "!");
							return true;
						} else if (status == false) {
							sender.sendMessage(ChatColor.RED + "Server is not muted!");
							return true;
						}
					}
				} else {
					if (status == false) {
						status = true;
						Bukkit.getServer().broadcastMessage(ChatColor.RED + "Server has been muted by " + ChatColor.GREEN + sender.getName() + "!");
						return true;
					} else if (status == true) {
						players.clear();
						status = false;
						Bukkit.getServer().broadcastMessage(ChatColor.RED + "Server has been unmuted by " + ChatColor.GREEN + sender.getName() + "!");
						return true;
					}
				}
			} else {
				sender.sendMessage(ChatColor.RED + "You do not have the required permissions!");
				return true;
			}
			
		}
		
		if (cmd.getName().equalsIgnoreCase("voice")) {
			
			if (status == false) {
				sender.sendMessage(ChatColor.RED + "The server is not muted!");
				return true;
			}
			
			if (args.length != 1) {
				sender.sendMessage(ChatColor.RED + "Invalid arguments!");
				return true;
			}
			
			Player target = (Player) Bukkit.getServer().getPlayer(args[0]);
			if (!(target instanceof Player)) {
				sender.sendMessage(ChatColor.RED + "Could not find player!");
				return true;
			}
			
			if (!(target.isOnline())) {
				sender.sendMessage(ChatColor.RED + "Could not find player!");
				return true;
			}
			Bukkit.getServer().broadcastMessage(ChatColor.GREEN + target.getName() + ChatColor.YELLOW + " has been voiced.");
			players.add(target.getName());
		}
		
		if (cmd.getName().equalsIgnoreCase("devoice")) {
			
			if (status == false) {
				sender.sendMessage(ChatColor.RED + "The server is not muted!");
				return true;
			}
			
			if (args.length != 1) {
				sender.sendMessage(ChatColor.RED + "Invalid arguments!");
				return true;
			}
			
			Player target = (Player) Bukkit.getServer().getPlayer(args[0]);
			if (!(target instanceof Player)) {
				sender.sendMessage(ChatColor.RED + "Could not find player!");
				return true;
			}
			
			if (!(target.isOnline())) {
				sender.sendMessage(ChatColor.RED + "Could not find player!");
				return true;
			}
			Bukkit.getServer().broadcastMessage(ChatColor.GREEN + target.getName() + ChatColor.YELLOW + " has been devoiced.");
			players.remove(target.getName());
		}	
		
		return true;
	}
	
	@EventHandler
	public void onPreprocess(PlayerCommandPreprocessEvent e) {
		
	    String command = e.getMessage().substring(1);
	    String[] cmd = command.split(" ");
		Player p = (Player) e.getPlayer();
		
		if (!(p.hasPermission("servermute.permissionoverride"))) {
			if (status == true) {
				if (!(players.contains(p.getName()))) {
					if (getConfig().getStringList("disabled-commands").contains(cmd[0])) {
						p.sendMessage(ChatColor.RED + "This command is disabled while servermute is active!");
						e.setCancelled(true);
					}
				}
			}
		}
		
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onChat(PlayerChatEvent e) {
		if (status == true) {
			Player p = (Player) e.getPlayer();
			if (!(p.hasPermission("servermute.bypass"))) {
				if (!(players.contains(p.getName()))) {
					p.sendMessage(ChatColor.RED + "Server is muted! You are not allow to speak.");
					e.setCancelled(true);
				}
			}
			
		}
	}

}
